$(document).ready(function () {

  // make green first 3 chars in phone number (top header)
  $('.top__header .top__header_phones a').each(function () {
    var phone_text = $(this).text();
    var first_chars = phone_text.substr(0, 3);
    var other_chars = phone_text.substr(3);
    $(this).html('<span class="accent_color">' + first_chars + '</span>' + other_chars);
  });

  // open/close catalog
  $('.headerMain_wrapper .headerMain .headerMain__catalog a.open-catalog').click(function (e) {
    e.preventDefault();
    $('.headerMain_wrapper .headerMain .headerMain__catalog .catalog-dropdown-menu').toggleClass('active');
  });

});
