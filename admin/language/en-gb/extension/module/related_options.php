<?php
// Heading
$_['heading_title']				= 'Related / Dependent options [RDO]';

// Text
$_['text_extension']				= 'Extensions';
$_['text_success']				= 'Success: You have modified module settings!';
$_['text_edit']					= 'Edit Module Settings';
$_['text_yes']					= 'Yes';
$_['text_no']					= 'No';
$_['text_php_max_input_vars_warning']		= 'The parameter &quot;max_input_vars&quot; of your server is [[php_max_input_vars]]. If you have more than 30 product options and the error &quot;Notice: Undefined index: active in ...&quot; appears, contact the server administrator to increase the php &quot;max_input_vars&quot; parameter to 2000-3000';
$_['text_cache_folder_not_found']		= 'Error. Cache directory not found';

// Column
//$_['column_name']				= 'Name';
//$_['column_sort_order']				= 'Sort Order';
//$_['column_action']				= 'Action';

// Entry
$_['entry_status']				= 'Status';
$_['entry_price_adjustment']			= 'Price adjustment on option change';
$_['entry_price_animate']			= 'Animate price adjustment';
$_['entry_show_out_of_stock']			= 'Show options with stock 0';
$_['entry_buy_out_of_stock']			= 'Buy product with stock 0';
$_['entry_decimal_places']			= 'Decimal Places';

// Help
$_['help_instructions']				= 'For editing related-to-option values open tab "Options" by a certain product at the Administration panel and fill in "Parent option" and "Parent option value".';
$_['help_price_adjustment']			= 'Note! Price adjustment according to selected option on the product page works with the following price format: dot as decimal separator and empty thousand separator.<br>Example: 2500.50<br>Taxes are not supported.';
$_['help_decimal_places']			= 'Only if you choose YES for price adjustment';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify module information!';
?>