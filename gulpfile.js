// Подключаем Gulp и все необходимые библиотеки
var gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
		autoprefixer   = require('autoprefixer'),
		ftp            = require('vinyl-ftp'),
		sass           = require('gulp-sass'),
		cssnano        = require('cssnano'),
		plumber 			 = require('gulp-plumber'),
		postcss        = require('gulp-postcss'),
		rename         = require('gulp-rename'),
		browsersync    = require('browser-sync').create(),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify-es').default,
		cleancss       = require('gulp-clean-css'),
		rsync          = require('gulp-rsync');

// Обновление страниц сайта на локальном сервере
// gulp.task('browser-sync', function(done) {
// 	browserSync.init({
// 		proxy: "opencart3.local",
// 		notify: false
// 	});
// 	done();
// });

function browserSync(done) {
	browsersync.init({
		proxy: "http://opencart3.local",
		notify: false
	});
	done();
}

// BrowserSync Reload
function browserSyncReload(done) {
	browsersync.reload();
	done();
}

function css() {
	return gulp.src('catalog/view/theme/stampik/stylesheet/sass/stylesheet.sass')
		.pipe(plumber())
		.pipe(sass({outputStyle: "expanded"}))
		.pipe(gulp.dest('catalog/view/theme/stampik/stylesheet/'))
		// .pipe(rename({ suffix: '-' + new Date().getSeconds() }))
		.pipe(postcss([autoprefixer(), cssnano()]))
		.pipe(gulp.dest('catalog/view/theme/stampik/stylesheet/'))
		.pipe(browsersync.stream())
}

function watchFiles() {
	gulp.watch('catalog/view/theme/stampik/stylesheet/sass/stylesheet.sass', css);
	gulp.watch('catalog/view/theme/stampik/template/**/*.twig', browserSyncReload);
	gulp.watch('catalog/view/theme/stampik/stylesheet/stylesheet.css', browserSyncReload);
	gulp.watch('catalog/view/theme/stampik/javascript/scripts.js', browserSync.reload);
}



// Компиляция stylesheet.css
// gulp.task('sass', function() {
// 	return gulp.src('catalog/view/theme/stampik/stylesheet/sass/stylesheet.sass')
// 		.pipe(sass({outputStyle: "expanded"}))
// 		.pipe(autoprefixer(['last 15 versions']))
// 		.pipe(cleancss())
// 		.pipe(gulp.dest('catalog/view/theme/stampik/stylesheet/'))
// 		.pipe(browserSync.stream())
// });


// Наблюдение за файлами
// gulp.task('watch', function(done) {
// 	gulp.watch('catalog/view/theme/stampik/stylesheet/sass/stylesheet.sass', css2);
// 	gulp.watch('catalog/view/theme/stampik/template/**/*.twig', browserSync.reload);
// 	gulp.watch('catalog/view/theme/stampik/js/**/*.js', browserSync.reload);
// 	gulp.watch('catalog/view/theme/stampik/libs/**/*', browserSync.reload);
// 	done();
// });

// Выгрузка изменений на хостинг
gulp.task('deploy', function() {
	var conn = ftp.create({
		host:      'hostname.com',
		user:      'username',
		password:  'userpassword',
		parallel:  10,
		log: gutil.log
	});
	var globs = [
	'catalog/view/theme/stampik/**'
	];
	return gulp.src(globs, {buffer: false})
	.pipe(conn.dest('/path/to/folder/on/server'));
});

gulp.task('sass2', gulp.series(css));

gulp.task('default', gulp.parallel(watchFiles, browserSync));
